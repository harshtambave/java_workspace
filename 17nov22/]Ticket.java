import java.util.Scanner;
class Ticket_Inspector{
	public static void main(String[] args){
		System.out.println("Enter the ticket number:");
		Scanner sc = new Scanner(System.in);
		int ticket_num = sc.nextInt();
		int last_digit = ticket_num%10;
		int rem_num = ticket_num/10;
		int check = rem_num%7;
		if(check==last_digit)
		{
			System.out.println("It is valid ticket number");
		}
		else
		{
			System.out.println("It is not valid ticket number");
		}
	}
}
