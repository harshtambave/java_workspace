abstract class candidate{
	String name,quali,post;
	candidate(String n,String q,String p)
	{
		name = n;
		quali = q;
		post =p;
	}
	void show()
	{
		System.out.println("Name: " +name + " Qualification: " +quali);
		System.out.println("Applied for post: " +post);
	}
	abstract void select();
}
class HR extends candidate{
	int id;
	HR(String n,String p,String q,int i)
	{
		super(n,p,q);
		id = i;
	}
void select()
{
	if (id > 0)
	{
		System.out.println("Congrats!!!Selected");
	}
	else if(id < 0)
	{
		System.out.println("Wait!!!On Hold");
	}
	else
	{
		System.out.println("Try Next Time");
	}
}
}
class abstractexample{
	public static void main(String[]args){
		candidate c = new HR("Harshal","BE","Java Dev",0);
		c.show();
		c.select();
		candidate c1 = new HR("Harshali","BE","Java Dev",10);
		c1.show();
		c1.select();
		candidate c2 = new HR("Prasad","BE","Java Dev",-1);
		c2.show();
		c2.select();
	}
}
