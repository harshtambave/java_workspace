class Parent1{

	void add(){

		System.out.println("add");

	}
}

class Parent2{

	void adding(){

		System.out.println("Adding");

	}
}
class Child extends Parent1, Parent2{

	public static void main(String[] args){

		Parent2 p2 = new Child();
		p2.add();
	}
}
// Java Does Not allow us to extend more than 1 class
