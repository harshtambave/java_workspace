interface A{

	default void add(){
	
		System.out.println("Adding");
	
	}
}
interface B extends A{

	default void add(){
		
		System.out.println("Add");
	}
}
class Demo implements B{

	public static void main(String[] args){

	A a = new Demo();
	a.add();

	B b = new Demo();
	b.add();

	}
}
