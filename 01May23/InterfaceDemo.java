interface Parent{

	 void property();
	 void responsibility();
}

class ElderChild implements Parent{
	
	public void property(){

		System.out.println("Proerty:: I dont want it, I will make it on my own");

	}

	public void responsibility(){

		System.out.println("Responsiblity:: I am following and its my duty to follow");

		}
}

class YoungerChild implements Parent{
	
	public void property(){

		System.out.println("Property:: I want it, I dont want to waste my time on making it on my own");

	}

	public void responsibility(){

		System.out.println("Responsibility:: Its my elder brother's Duty :) He will look into it");

		}
}

class House{

	public static void main(String[] args){

		Parent p1 = new ElderChild();
		p1.property();
		p1.responsibility();
		Parent p2 = new YoungerChild();
		p2.property();
		p2.responsibility();

	}
}
