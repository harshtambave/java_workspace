interface Parent{
	
	default void Discipline(){
		System.out.println("You Cannot override it");
	}
}
class Child implements Parent{
	public void Discipline(){

			System.out.println("I will override it");
	}
}
class Demo{
	public static void main(String[]args){
		Parent p = new Child();
		p.Discipline();
	}
}

