import java.io.*;
class PalindromeDemo{
	public static void main(String...args)throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Number: ");
		int a = Integer.parseInt(br.readLine());
	//	int a = 121;
		int temp = a;
		int rev = 0;
		int rem;
		while (temp!=0){
			rem = temp%10;
			rev = rev*10+rem;
		       temp = temp/10;
		}
 		if(rev == a){
		System.out.println(a+ " is a Palindrome");
		}
		else{
		System.out.println(a+  " is not a Palindrome");
		}
	}
}	
