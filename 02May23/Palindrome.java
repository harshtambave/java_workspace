class PalindromeDemo{

	public static void main(String[] args){

		int a = 444;
		int temp = a;
		int rev = 0;
		int rem;

		while(temp!=0){

			rem = temp%10;
			rev = rev*10+rem;
			temp = temp/10;
		}
		if(rev == a){

			System.out.println("Its a palindrome");

		}
		else{
			System.out.println("Not a Palindrome");
		
		}

	}
}

