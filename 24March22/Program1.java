class Parent{
	void p1(){
		System.out.println("in P1");
	}
 }
class Child extends Parent{

	void p2(){
		System.out.println("In P2");
	}
}
class Demo{
	void m1(Parent p){
		System.out.println("M1 Parent");
              }
	void m1(Child p){
		System.out.println("M1 Child");
	        p.p1();
		p.p2();
	}
	public static void main(String[]args){
		Demo d = new Demo();
		d.m1(new Child());
	}
}

