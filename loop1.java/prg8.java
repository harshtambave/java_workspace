import java.util.*;

class Program8{

	public static void main(String[]args){

		Scanner obj= new Scanner(System.in);
		
		System.out.println("Enter Lower Limit");
		int lowlimit=obj.nextInt();
		System.out.println("Enter Upper Limit");
		int uplimit=obj.nextInt();

		for(int row=lowlimit;row<=uplimit;row++){
			if(row%10==0){
				System.out.println(row+" ");
			}
		}
	}
}
	
