import java.util.*;

class Program5{

	public static void main(String[]args){

		Scanner obj = new Scanner(System.in);

		System.out.println("Enter no. of rows");
		int num = obj.nextInt();

	for(int row=1;row<=num;row++){
		int x=1;

		for(int col=1;col<=row;col++){
			System.out.print(row*x+ " ");
			x++;
		}
		System.out.println();
	}
     } 
}
