class StaticDemo{
	static{
		System.out.println("In Static Block");
	}

	public static void main(String[] args){

		System.out.println("In Main Block");
		
	}
	
}
