class HashCode{

	public static void main(String[] args){

		String str1 = "Core2Web";
		String str2 = "Core2Web";
		String str3 = new String("Core2Web");

		System.out.println(str1.hashCode());
		System.out.println(str2.hashCode());
		System.out.println(str3.hashCode());
	}
}
