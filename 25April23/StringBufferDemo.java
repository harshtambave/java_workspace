class StringBufferDemo{

	public static void main(String[] args){

		//I am Synchronous and I pass only one thraed at a time due to this I am slow.
		//but I am thread safe.

		String str = "Bhavakaiani";

		StringBuffer sb = new StringBuffer("Bhavakaiani");
	
		System.out.println(sb);
	
		System.out.println(str.substring(4,7));
	
		System.out.println(str.substring(4));
		
		sb.delete(4,7);
		
		System.out.println(sb);
	}
}
