class StringBuilderDemo{


	// I am Asynchronous and I am not Thread safe because I pass multiple theards at the same time.
	// But I am Fast.

	public static void main(String[] args){

		StringBuilder sb = new StringBuilder("Core2Web");

		System.out.println(sb);
		
		sb.delete(2,4);

		System.out.println(sb);
	}
}
