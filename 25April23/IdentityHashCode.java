class IdentityHashCode{

	public static void main(String[] args){

		String str1 = "Core2Web";
		String str2 = "Core2Web";
		String str3 = new String("Core2Web");
		str3.intern();

		System.out.println(System.identityHashCode(str1));
		System.out.println(System.identityHashCode(str2));
		System.out.println(System.identityHashCode(str3));
	
		System.out.println(System.identityHashCode(str3.intern()));
	}
}
