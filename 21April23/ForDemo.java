import java.util.*;
class ForDemo{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter No. of Rows::");
		int row = sc.nextInt();
 		
		char ch = 'A';
		char ch1 = 'a';
		int num = 1;
		for (int i=1;i<=row;i++){
			for (int j =1;j<=row;j++){
				if (num%2 == 1){
					System.out.print(" "+ ch);
				}else{
					System.out.print(" "+ ch1);
				}
				ch++;
				ch1++;
				num++;
			}

			System.out.println();
		}
	}
}
