class StringDemo{
	public static void main(String[]args){
		StringBuffer sb = new StringBuffer("Young");
		System.out.println(sb);
		sb.append("  Person");
		System.out.println(sb);
		sb.insert(6,"Intellegent");
		System.out.println(sb);
		System.out.println(sb.delete(6,12));
		sb.replace(0,5, "Wizskill");
		System.out.println(sb);
		sb.setCharAt(0,'w');
		System.out.println(sb);
		System.out.println(sb.reverse());
	}		
}
