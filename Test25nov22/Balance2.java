class balancedemo{
	int id, amount;
	String name, type;
	void accept(int id, String name, String type, int am)
	{
		this.id = id;
		this.amount = am;
		this.name=name;
		this.type=type;
	}
	void examine1()
	{
		if(amount>10)
		{
			System.out.println("account balance is Okay");
		}
		else if(amount<10)
		{
			System.out.println("account balance is low");
		}
	}
	void examine2()
	{
		if(amount>100)
		{
			System.out.println("savings account balance is Okay");
		}
		else if(amount<100)
		{
			System.out.println("savings account balance is low");
		}
	}
	void show()
	{
		System.out.println("Id: " +id+  " Name:" +name);
		System.out.println("Type: " +type+  " Amount: "  +amount);

	}
	public static void main(String[]args){
		balancedemo b1 = new balancedemo();
		b1.accept(1, "Harshal","Current account",9);
		b1.show();
		b1.examine1();
		balancedemo b2 = new balancedemo();
		b2.accept(2, "Harshali","Current account",11);
		b2.show();
		b2.examine1();
		balancedemo b3 = new balancedemo();
		b3.accept(3, "Karan","Savings account",99);
		b3.show();
		b3.examine2();
		balancedemo b4 = new balancedemo();
		b4.accept(4, "Prasad","Savings account",145);
		b4.show();
		b4.examine2();
	}
}
