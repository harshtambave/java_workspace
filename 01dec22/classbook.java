class Book{
	String title;
	int Noofpages;
	void setTitle(String title)
	{
		this.title = title;
	}
	void setNoofpages(int p)
	{
		Noofpages = p;
	}
	public String getTitle()
	{
		return title;
	}
	public int getNoofpages()
	{
		return Noofpages;
	}
}
class Textbook extends Book{
	String gradelevel;
	
	void setGradelevel(String g)
	{
		this.gradelevel = g;
	}
	public String getGradelevel()
	{
		return gradelevel;
	}
	}
	class DemoBook{
		public static void main(String[]args){
			Book b = new Book();
			b.setTitle("Ramayana");
			b.setNoofpages(500);

			System.out.println("Book Name: " +b.getTitle());
			System.out.println("Number of Pages: " +b.getNoofpages());

			Textbook tb = new Textbook();
			tb.setTitle("Chava");
			tb.setNoofpages(200);
			tb.setGradelevel("Historical Biography");

			System.out.println("Book Name: " +tb.getTitle());
			System.out.println("Number of pages: " +tb.getNoofpages());
			System.out.println("Book Gradelevel: " +tb.getGradelevel());
		}
	}

