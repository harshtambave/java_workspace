class OverloadingDemo{
	void add(int a, int b){
		System.out.println("sum:: "+ (a+b));
	}
	void add(int a, int b, int c){
		System.out.println("sum:: "+ (a+b+c));
	}
	public static void main(String[] args){
		OverloadingDemo o = new OverloadingDemo();
		o.add(5,7);
		o.add(1,2,31);
	}
}
