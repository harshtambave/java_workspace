class encapsulation{
	private String name,dept;
	private int id;
	private float sal;

	public void setName(String n)
	{
		name = n;
	}
	public String getName()
	{
		return name;
	}
	public void setDept(String d)
	{
		dept = d;
	}
	public String getDept()
	{
		return dept;
	}
	public void setId(int i)
	{
		id = i;
	}
	public int getId()
	{
		return id;
	}
	public void setSal(float s)
	{
		sal = s;
	}
	public float getSal()
	{
		return sal;
	}
	
	public static void main(String[]args){
		encapsulation e = new encapsulation();
		e.setId(101);
		e.setName("Harshal");
		e.setDept("IT");
		e.setSal(150000);
		System.out.println("Id: " +e.getId());
		System.out.println("Name: " +e.getName());
		System.out.println("Department: " +e.getDept());
		System.out.println("Salary: " +e.getSal());
	}
}
