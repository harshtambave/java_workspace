interface player{
	void play();
	}
class child implements player
{
	public void play(){
		System.out.println("Children play on ground");
	}
}
class Musicians implements player
{
	public void play(){
		System.out.println("Musicians play music on Instrument");
	}
}
class actor implements player
{
	public void play(){
		System.out.println("Actor plays a role in Movie");
	}
}
class Interface2{
	public static void main(String[]args){
		player p = new child();
		p.play();
		Musicians m = new Musicians();
		m.play();
		actor a = new actor();
		a.play();
	}
}
