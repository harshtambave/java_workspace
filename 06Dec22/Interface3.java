interface Turner{
	void turn();
}
class pancake implements Turner{
	public void turn(){
		System.out.println("Turner");
	}
}
class Leaf implements Turner{
	public void turn(){
		System.out.println("Changing Colours");
	}
}
class Page implements Turner{
	public void turn(){
		System.out.println("Flipping");
	}
}
class Demo_turner{
	public static void main(String[]args){
		Turner t = new pancake();
		t.turn();
		Leaf l  = new Leaf();
		l.turn();
		Page p = new Page();
		p.turn();
class Leaf implements Turner{
	public void turn()
	{
		System.out.println("Changing Colours");
	}
}
	}
}
