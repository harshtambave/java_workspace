package com.java;
import java.sql.*;
public class JdbcDemo {

	public static void main(String[] args)throws Exception {

		String url = "jdbc:mysql://localhost:3306//startup";
		String uname = "root";
		String pass = "";
		String query = "Select * from employee";
		Class.forName("com.mysql.jdbc.Driver");
		Connection con = DriverManager.getConnection(url,uname,pass);
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		rs.next();
		String name = rs.getString("Ename");
		
		System.out.println(name);
	
		st.close();
		con.close();
 	
	}
	
	

}
