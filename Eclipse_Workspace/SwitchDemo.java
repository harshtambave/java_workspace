import java.io.*;
class SwitchDemo {

	public static void main(String[] args) throws Exception{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Brands: 1.Nike 2.Addidas 3.Puma 4.Reebok");
		System.out.println("Your Brand Choice : ");
		int a = Integer.parseInt(br.readLine());
		switch(a) {
		case 1 : System.out.println("Nike : Just Do It");
		break;
		case 2 : System.out.println("Addidas : Imposible is nothing");
		break;
		case 3 : System.out.println("Puma : Forever Faster");
		break;
		case 4 : System.out.println("Reebok : I am what I am");
		break;
		default  :  System.out.println("Entered wrong input");
		}
	}
}