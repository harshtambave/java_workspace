import java.io.*;
class LeapYear{
	public static void main(String... args) throws Exception{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		System.out.println("Enter Year: ");
		String str =br.readLine(); 
		int year = Integer.parseInt(str);
		if (year%4==0) {
			System.out.println(year+ " is a Leap Year");
		}
		else {
			System.out.println(year+ " is not a Leap Year");
		}
	}
}