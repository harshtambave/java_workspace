import java.io.*;
class PrimeNumberDemo {

	public static void main(String[] args) throws Exception {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		System.out.println("Enter Number: ");
		String str =br.readLine(); 
		int a = Integer.parseInt(str);
		int b = 0;
		for (int i=2; i<=a-1;i++) {
			if (a%i==0) {
				b = b+1;
			}
		}
		if (b==0) {
			System.out.println(a+ " is Prime number");
		}
		else {
			System.out.println(a+ " is not Prime number");
		}
	}
}