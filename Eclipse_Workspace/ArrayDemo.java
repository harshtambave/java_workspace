import java.util.Arrays;

public class ArrayDemo {

	public static void main(String[] args) {
		String[] arr = new String[10];
		arr[0] = "harshal";
		arr[1] = "Sanket";
		arr[2] = "Prasad";
		arr[3] = "Satyam";
		arr[4] = "Vaibhav";
		arr[5] = "Prathamesh";
		arr[6] = "Mangesh";
		arr[7] = "Vishal";
		arr[8] = "Omkar";
		arr[9] = "prashant";
		//Another Program
		int[] ar = {1,2,3,4,5};
		String str = Arrays.toString(ar);
		System.out.println(str);
		//Another Program
		System.out.println("-------------------------------------------------------------");
		for (int i=0; i<=arr.length;i++)
		{
			System.out.println(arr[i]);
		}
	}
	
}