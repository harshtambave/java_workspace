import java.io.*;
public class DesignationDemo {

	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Experience:");
		int exp= Integer.parseInt(br.readLine());
		
		
		
		if (exp>=0 && exp<2) {
			System.out.println("Associate Engineer");
		}
		else if(exp>=2 && exp<=4) {
			System.out.println("Software Engineer");
		}
		else if(exp>=5 && exp<=7) {
			System.out.println(" Sr.Software Engineer");
		}
		else if(exp>7 && exp<=10) {
			System.out.println("Manager");
		}
		else {
			System.out.println("Employee is having experience in between 4 to 5 years");
		}
			
	}

}
