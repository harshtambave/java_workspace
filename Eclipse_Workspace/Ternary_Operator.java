import java.sql.SQLNonTransientConnectionException;

class Ternary_Operator {

	public static void main(String[] args) {
		
		int a = 15;
		int b = 20;
		String c = (a<b) ?  "a is small" :"a is large";
		
		System.out.println(c);
	}

}
