import java.util.Scanner;
public class Exceptiondemo {
		public static void main(String[]args) {
			System.out.println("open the file");
			Scanner sc = new Scanner(System.in);
			System.out.println("enter the value of a");
			int a = sc.nextInt();
			try {
				int n = 45/a;
				System.out.println("value of n : " +n);
			} 
			catch (Exception e)
			{
				System.out.println(e);
				//e.printStackTrace() .....Special method to print the Exception
			}
	         System.out.println("Close the file");
		}
		
	}
