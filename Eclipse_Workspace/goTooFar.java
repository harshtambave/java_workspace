public class goTooFar
{
	public static void main(String[] args)
	{
		final int[] Arr = {2, 3, 5, 7, 11};
		int i = 0;

		try
		{
			while(true)
			{
				System.out.println(i + ": " + Arr[i]);
				i++;
			}
		}
		catch(ArrayIndexOutOfBoundsException tooFar)
		{
			System.out.println("Now you've gone too far");
		}
	}
}