abstract class HotelMenu{
	
	void token(){

	System.out.println("Collect your token from here");

	}

	abstract void greenToken();
	abstract void redToken();

}

class Starters extends HotelMenu{

	void greenToken(){

	System.out.println("Veg Starters");
	
	}
	void redToken(){

	System.out.println("Non-Veg Starters");

	}
}

class MainCourse extends HotelMenu{

	void greenToken(){

	System.out.println("Veg main Course");

	}
	void redToken(){

	System.out.println("Non-Veg Main Course");

	}
}

class PlaceOrder{

	public static void main(String[] args){
		
		HotelMenu m1 = new Starters();
		m1.token();
		m1.greenToken();
		m1.redToken();

		HotelMenu m2 = new MainCourse();
		m2.token();
		m2.greenToken();
		m2.redToken();
	}
}
