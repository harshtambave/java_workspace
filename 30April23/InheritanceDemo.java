class Parent{

	Parent(){

		System.out.println("In parent Constructor");

	}
}

	class Child extends Parent{

		Child(){

		System.out.println("In child Constructor");

		}
	}

		class Main{
			public static void main(String[] args){

				Parent p = new Child();

			}
		}
