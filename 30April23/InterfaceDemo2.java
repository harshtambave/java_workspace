interface A{

	void m1();
}
interface B extends A{

	void m2();
}
class Demo implements B{
	public void m1(){
		System.out.println("In m1");
	}

	public void m2(){

		System.out.println("In m2");

	}
	
	public static void main(String[] args){

		B a = new Demo();
		a.m1();
		a.m2();
	}
}

