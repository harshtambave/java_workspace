class InstanceBlockDemo2{

	int x = 10;

	{

		System.out.println("In Instance Block 1");

	}

	public static void main(String[] args){

		InstanceBlockDemo2 ib = new InstanceBlockDemo2();

		System.out.println(ib.x);

	}
	{
		x = 20;
		System.out.println("In Instance Block 2");
	}
}
