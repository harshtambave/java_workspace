class Parent{

	Parent(){

		System.out.println("In Parent");
	}

	static{

		System.out.println("In Static - P");

	}
}

	class Child extends Parent{

		Child(){

		System.out.println("In Child");
		}

		static { 


		System.out.println("In Static - C");

		}
	}
	
	class Demo{
		
		public static void main(String[] args){

			Parent p = new Parent();
			Child  c = new Child();


		}
	}

